const express = require('express')
const session = require('express-session')
const cors=require("cors")
const cookieParser = require("cookie-parser");


require('dotenv').config()
const app = express()
const port = process.env.PORT || 8181

app.use(cookieParser());

// Configuration if we had cross origin enabled.
// let sess = {
//     secret: 'our litle secret',
//     resave: false,
//     proxy: true,
//     saveUninitialized: true,
//     cookie: {
//         secure: true,
//         sameSite: 'none'
//     }
// }

let sess = {
    secret: 'our litle secrett',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
}
// let sess = {
//     secret: 'our litle secret',
//     saveUninitialized: true,
//     resave: false,
//     proxy: true,
//     name:"app",
//     cookie: {
//         httpOnly: true,
//     }
// }

app.use(session(sess))

//Some configurations
app.use(express.urlencoded({extended : true}));
app.use(cors({
 methods:["GET", "POST"],
  credentials: true, 
  origin: ['http://localhost:3000','http://localhost:3001']
}))


// to allow delete 
// app.use(cors({
//     origin: ['http://localhost:3000'], // Replace with your frontend URL
//     methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'], // Explicitly allow all required methods
//     allowedHeaders: ['Content-Type', 'Authorization'], // Ensure headers like Content-Type are allowed
//     credentials: true, // Allow cookies and credentials
// }));
// app.options('*', cors()); // Handle preflight requests




app.use(express.json());
app.use(express.urlencoded({ extended: true }));


// app.use(
//     cors({
//         origin: 'http://localhost:3000', // Frontend origin
//         methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'], // Allow DELETE
//         credentials: true, // Allow cookies
//     })
// );
// app.options('*', (req, res) => {
//     res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
//     res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
//     res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
//     res.header('Access-Control-Allow-Credentials', 'true');
//     res.status(204).send(); // No Content
// });

const corsOptions = {
    origin: ['http://localhost:3000'], // Add your frontend URL
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'], // Allow DELETE
    allowedHeaders: ['Content-Type', 'Authorization'], // Allow necessary headers
    credentials: true, // Allow cookies
};

app.use(cors(corsOptions));

// Handle preflight requests globally
app.options('*', (req, res) => {
    res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.status(204).send(); // Respond with 204 No Content
});





const novice = require('./routes/novice')
const users = require('./routes/users')
const upload = require('./routes/upload')
const profile = require('./routes/profile')
const search = require('./routes/search')
const mealplan = require('./routes/mealplan')
const shoppinglist = require('./routes/shoppinglist')
const comments = require('./routes/comments')
const ratings = require('./routes/ratings')

app.use('/novice', novice)
app.use('/users', users)
app.use('/upload', upload)
app.use('/profile', profile)
app.use('/search', search)
app.use('/mealplan', mealplan)
app.use('/shoppinglist', shoppinglist)
app.use('/comments', comments)
app.use('/ratings', ratings)

const path = require('path')
console.log(__dirname)
app.use(express.static(path.join(__dirname, "build")))
app.use(express.static(path.join(__dirname, "uploads")))

app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "build", "index.html")) 
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
