const express = require("express")
const profile = express.Router();
const DB = require('../db/dbConn.js')

profile.get('/profileData:userID', async (req, res, next) => {
    try {
        const queryResult = await DB.userProfile();
        res.json(queryResult);
        if (req.session.username) {
            res.send(`Logged in as ${req.session.username}`);
            console.log(`Logged in as ${req.session.username}`);
        } //else {
        //     res.redirect('/login');
        // }
    }
    catch (err) {
        console.error('Error fetching user:', err);
        res.sendStatus(500);
        next();
    }
});

profile.post('/profileData', async (req, res, next) => {
    try{
    const userID = req.body.userID
    const username = req.body.username
    const email = req.body.email
    const password = req.body.password
    const queryResult = await DB.userProfile(userID, username, email, password);
    res.json(queryResult);
    }
    catch (err) {
        console.error('Error fetching user:', err);
        res.sendStatus(500);
        next()
    }
});

// user.post('/api/update-profile', (req, res) => {
//     //const userID = req.body.userID
//     const username = req.body.username
//     const password = req.body.password
//     const email = req.body.email
//     const queryResult = await DB.userProfile(userID, username, email, password);
//     User.findByIdAndUpdate(req.userId, { username, email, password }, { new: true }, (err, user) => {
//         if (err) return res.status(500).send(err);
//         res.json(user);
//     });
//     if (username && password && email) {
//         const queryResult = await DB.userProfile(userID, username, email, password);
//         if (queryResult.affectedRows) {
//             res.statusCode = 200;
//             res.send({ status: { success: true, msg: "New user created" } })
//             console.log("New user added!!")
//         }
//     }
//     else {
//         res.statusCode = 200;
//         res.send({ status: { success: false, msg: "Input element missing" } })
//         console.log("A field is missing!")
//     }
//     res.end();
    
// });

module.exports = profile