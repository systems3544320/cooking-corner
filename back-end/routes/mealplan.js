const express = require("express");
const mealplan = express.Router();
const DB = require("../db/dbConn"); // Adjust the path if needed

// Route to fetch meal plan recipes
mealplan.get('/fetchMealPlan', async (req, res, next) => {
    try {
        // Fetch recipes from MEAL_PLAN table joined with RECIPES
        const query = `
            SELECT mp.planID, mp.date, mp.dayOfWeek, r.recipeID, r.title, r.image, r.cookingTime, r.calories, r.mealType
            FROM MEAL_PLAN mp
            JOIN RECIPES r ON mp.r_recipeID = r.recipeID
            ORDER BY FIELD(mp.dayOfWeek, 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'), mp.date
        `;
        const results = await DB.query(query);
        console.log("Fetched Meal Plan Data:", results);
        res.status(200).json(results);
    } catch (err) {
        console.error("Error fetching meal plan:", err);
        res.status(500).send("Failed to fetch meal plan");
    }
});

// mealplan.delete('/removeFromMealPlan', async (req, res, next) => {
//     const { planID } = req.body; // Use planID to identify the specific entry

//     if (!planID) {
//         return res.status(400).send("Missing planID");
//     }

//     try {
//         // Delete the entry from the MEAL_PLAN table
//         await DB.query(`DELETE FROM MEAL_PLAN WHERE planID = ?`, [planID]);
//         res.status(200).send("Recipe removed from meal plan");
//     } catch (err) {
//         console.error("Error removing from meal plan:", err);
//         res.status(500).send("Failed to remove recipe from meal plan");
//     }
// });

// mealplan.delete('/removeFromMealPlan', async (req, res, next) => {
//     const { recipeID } = req.body;

//     if (!recipeID) {
//         return res.status(400).send("Recipe ID is required to remove from meal plan.");
//     }

//     try {
//         // Remove the recipe from the meal plan table in your database
//         const result = await DB.query(
//             `DELETE FROM MEAL_PLAN WHERE r_recipeid = ?`,
//             [recipeID]
//         );

//         if (result.affectedRows > 0) {
//             res.status(200).send("Recipe successfully removed from meal plan.");
//         } else {
//             res.status(404).send("Recipe not found in meal plan.");
//         }
//     } catch (err) {
//         console.error("Error removing recipe from meal plan:", err);
//         res.status(500).send("Failed to remove recipe from meal plan.");
//     }
// });

mealplan.delete("/removeFromMealPlan", async (req, res) => {
    const { recipeID } = req.body; // Assuming `recipeID` is passed in the request body

    if (!recipeID) {
        return res.status(400).send("Recipe ID is required");
    }

    try {
        await DB.query("DELETE FROM MEAL_PLAN WHERE r_recipeID = ?", [recipeID]);
        res.status(200).send("Recipe removed from meal plan");
    } catch (err) {
        console.error("Error removing recipe from meal plan:", err);
        res.status(500).send("Failed to remove recipe from meal plan");
    }
});


// Update day of the week for a meal plan recipe
// mealplan.put("/updateDay", async (req, res) => {
//     const { planID, dayOfWeek } = req.body;

//     if (!planID || !dayOfWeek) {
//         return res.status(400).send("planID and dayOfWeek are required");
//     }

//     try {
//         await DB.query(
//             `UPDATE MEAL_PLAN SET dayOfWeek = ? WHERE planID = ?`,
//             [dayOfWeek, planID]
//         );
//         res.status(200).send("Day of the week updated successfully");
//     } catch (err) {
//         console.error("Error updating day of the week:", err);
//         res.status(500).send("Failed to update day of the week");
//     }
// });



module.exports = mealplan;
