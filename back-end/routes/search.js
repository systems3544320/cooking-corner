const express = require("express");
const axios = require("axios");  // ...........
const search = express.Router();
const DB = require('../db/dbConn');  // .................

const APIurl = "https://api.edamam.com/api/recipes/v2?type=public&app_id=21e0b7ea&app_key=238825a094b69f9f29ed807f2b738105&diet=balanced";


search.get('/fetchRecipes', async (req, res, next) => {  // FROM APII
    try {
        const response = await axios.get(
            `https://api.edamam.com/api/recipes/v2`,
            {
                params: {
                    type: 'public',
                    app_id: '21e0b7ea',
                    app_key: '238825a094b69f9f29ed807f2b738105',
                    diet: 'balanced'
                }
            }
        );

        const recipes = response.data.hits;

        // Iterate through each recipe and extract ingredients
        for (const recipe of recipes) {
            const rec = recipe.recipe;
            const recipeID = await DB.saveRecipe({
                title: rec.label,
                calories: rec.calories,
                image: rec.image,
                totalTime: rec.totalTime,
                mealType: rec.mealType[0] || 'unknown'
            });

            // console.log("Fetched recipeID:", recipeID);

            const ingredients = recipe.recipe.ingredients; // Extract the ingredients array

            for (const ingredient of ingredients) {

                // Insert the ingredient into the `ingredients` table
                await DB.saveIngredient({
                     
                        name: ingredient.food,             // Ingredient name
                        quantity: ingredient.quantity || null, // Quantity (nullable)
                        measure: ingredient.measure || null   // Measure (nullable)
                }, recipeID
                );
            }
        
        }

        // Fetch ratings from the database
        // for (const recipe of recipe) {
        //     const [dbRecipe] = await DB.query(`SELECT rating FROM RECIPES WHERE title = ? LIMIT 1`, [recipe.label]);
        //     recipe.rating = dbRecipe ? dbRecipe.rating : null; // Include the rating or null
        // }

        res.json(recipes); // Return the fetched recipes to the frontend
    } catch (err) {
        console.error("Error fetching recipes or saving ingredients:", err);
        res.status(500).send("Failed to fetch recipes");
    }
});  // ....................

// search.get('/fetchRecipes', async (req, res, next) => {   /// FROM DATABASE
//     try {
//         const query = `
//             SELECT recipeID, title, cookingTime, image, mealType, calories, dateAdded, rating
//             FROM RECIPES
//             ORDER BY recipeID ASC
//         `;
//         const results = await DB.query(query);
//         res.status(200).json(results);
//     } catch (err) {
//         console.error("Error fetching recipes from database:", err);
//         res.status(500).send("Failed to fetch recipes");
//     }
// });

// search.get('/fetchIngredients', async (req, res, next) => {
//     try {
//         const query = `
//             SELECT ingredientID, r_recipeID, name, quantity, measure
//             FROM INGREDIENT
//             ORDER BY ingredientID ASC
//         `;
//         const results = await DB.query(query);
//         res.status(200).json(results);
//     } catch (err) {
//         console.error("Error fetching ingredients from database:", err);
//         res.status(500).send("Failed to fetch ingredients");
//     }
// });   /////////////



    search.post('/saveToMealPlan', async (req, res, next) => {
        const { recipeTitle, date, userID, dayOfWeek } = req.body;

        if (!recipeTitle || !date || !userID || !dayOfWeek) {
            console.error("Missing recipeTitle, date, or userID");
            return res.status(400).send("Missing recipeTitle, date, or userID");
        }

        try {
            // Fetch the recipeID from the RECIPES table using the recipeTitle
            const query = `SELECT recipeID FROM RECIPES WHERE title = ? LIMIT 1`;
            const result = await DB.query(query, [recipeTitle]);

            if (result.length === 0) {
                console.error("Recipe not found with title:", recipeTitle);
                return res.status(404).send("Recipe not found");
            }

            const recipeID = result[0].recipeID;
            console.log("Fetched recipeID:", recipeID); // Debug log

            // Insert into MEAL_PLAN table
            await DB.query(
                `INSERT INTO MEAL_PLAN (u_userID, r_recipeID, date, dayOfWeek) VALUES (?, ?, ?, ?)`,
                [userID, recipeID, date, dayOfWeek]
            );

            res.status(200).send("Recipe added to meal plan");
        } catch (err) {
            console.error("Error saving to meal plan:", err);
            res.status(500).send("Failed to save to meal plan");
        }
    });




// search.get('/saveRecipes', async (req, res, next) => {  /// API .................
//     try {
//         // Fetch data from the Edamam API
//         const response = await axios.get(APIurl);
//         const recipes = response.data.hits;

//         // Save each recipe to the database
//         for (const recipeData of recipes) {
//             const recipe = recipeData.recipe;
//             await DB.saveRecipe({
//                 title: recipe.label,
//                 description: recipe.ingredientLines.join(", "),
//                 image: recipe.image,
//                 cookingTime: recipe.totalTime || 0,
//                 mealType: recipe.mealType[0] || 'unknown'
//             });
//         }

//         res.status(200).send("Recipes saved successfully");
//     } catch (err) {
//         console.error(err);
//         res.status(500).send("Error saving recipes to the database");
//         next();
//     }
// });  // .....................

search.get('/searchIngredient', async (req, res, next) => {
    try {
        const queryResult = await DB.searchIngredient();
        res.json(queryResult)
    }
    catch (err) {
        console.log(err)
        res.sendStatus(500)
        next()
    }
});

search.get('/search', async (req, res, next) => {
    const { query } = req.query;

    if (!query) {
        return res.status(400).send("Search query is required");
    }

    try {
        // Search in RECIPES table
        const recipeQuery = `
            SELECT * FROM RECIPES 
            WHERE title LIKE ? 
               OR mealType LIKE ? 
               OR (cookingTime <= ? AND ? REGEXP '^[0-9]+$')
               OR (calories <= ? AND ? REGEXP '^[0-9]+$')
        `;
        
        // Search in INGREDIENT table for matching ingredient names
        const ingredientQuery = `
            SELECT r.* FROM RECIPES r
            JOIN INGREDIENT i ON r.recipeID = i.r_recipeID
            WHERE i.name LIKE ?
        `;

        const searchTerm = `%${query}%`;

        // Execute both queries
        const [recipeResults, ingredientResults] = await Promise.all([
            DB.query(recipeQuery, [searchTerm, searchTerm, query, query, query, query]),
            DB.query(ingredientQuery, [searchTerm])
        ]);

        // Merge and remove duplicates
        const allResults = [...recipeResults, ...ingredientResults].filter((value, index, self) =>
            index === self.findIndex((t) => t.id === value.id)
        );

        res.json(allResults);
    } catch (err) {
        console.error("Error during search:", err);
        res.status(500).send("Error during search");
    }
});


module.exports = search

