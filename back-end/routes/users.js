const express = require("express")
const users = express.Router();
const DB = require('../db/dbConn.js')

users.post('/login', async (req, res, next) => {
    try {
        console.log(req.body);
        const username = req.body.username;
        const password = req.body.password;
        if (username && password) {
            const queryResult = await DB.AuthUser(username, password)
            if (queryResult.length > 0) {
                if (password === queryResult[0].password) {
                    req.session.user = queryResult
                    req.session.logged_in = true
                    res.statusCode = 200;
                    res.json({ user: queryResult[0], status: { success: true, msg: "Logged in" } })
                    req.session.username = username;
                   // res.redirect('/profile');
                } else {
                    res.statusCode = 200;
                    res.json({ user: null, status: { success: false, msg: "Password is incorrect" } })
                    console.log("INCORRECT PASSWORD")
                }
            } else {
                res.statusCode = 200;
                res.send({ user: null, status: { success: false, msg: "Username not registered" } })
            }
        }
        else {
            res.statusCode = 200;
            res.send({ logged: false, user: null, status: { success: false, msg: "Input element missing" } })
            console.log("Please enter Username and Password!")
        }
        res.end();
    } catch (err) {
        console.log(err)
        res.sendStatus(500)
        next()
    }
});

users.get('/session', async (req, res, next) => {
    try {
        res.json(req.session)
    } catch (error) {
        res.sendStatus(500)
    }
})

//Inserts a new user in our database id field are complete
users.post('/register', async (req, res, next) => {
    try {
        // dodati user type
        const username = req.body.username
        const password = req.body.password
        const email = req.body.email
        const userType = req.body.userType
        if (username && password && email && userType) {
            const queryResult = await DB.AddUser(username, email, password, userType);
            if (queryResult.affectedRows) {
                res.statusCode = 200;
                res.send({ status: { success: true, msg: "New user created" } })
                console.log("New user added!!")
            }
        }
        else {
            res.statusCode = 200;
            res.send({ status: { success: false, msg: "Input element missing" } })
            console.log("A field is missing!")
        }
        res.end();
    } catch (err) {
        console.log(err)
        res.statusCode = 500;
        res.send({ status: { success: false, msg: err } })
        next()
    }

});

users.post('/preferences', async (req, res) => {
    const { user_input } = req.body;
  
    try {
      await DB.AddPreferences( user_input);
      res.json({ success: true, msg: "Preferences saved successfully" });
    } catch (err) {
      console.error("Error saving preferences:", err);
      res.status(500).json({ success: false, msg: "Error saving preferences" });
    }
});

users.post('/pagePreference', async (req, res) => {
    const { user_input } = req.body;

    try {
        // Add the like or dislike to the preferences table
        await DB.AddPreferences(user_input); // Assuming DB.AddPreferences handles inserting the data
        res.json({ success: true, msg: "Page preference saved successfully" });
    } catch (err) {
        console.error("Error saving page preference:", err);
        res.status(500).json({ success: false, msg: "Error saving page preference" });
    }
});

users.get('/pagePreferenceCount', async (req, res) => {
    try {
        const preferenceCounts = await DB.GetPreferences();
        const counts = preferenceCounts.reduce((acc, row) => {
            acc[row.user_input] = row.count;
            return acc;
        }, {});

        res.json({
            likeCount: counts['like'] || 0,
            dislikeCount: counts['dislike'] || 0
        });
    } catch (err) {
        console.error('Error fetching page preference counts:', err);
        res.status(500).json({ error: 'Failed to fetch page preference counts' });
    }
});

users.get('/count', async (req, res) => {
    try {
      const preferenceCounts = await DB.GetPreferences();
      console.log("Preference Counts from DB:", preferenceCounts); // Log the result

      const counts = preferenceCounts.reduce((acc, row) => {
        acc[row.user_input] = row.count;
        return acc;
      }, {});
  
      res.json({
        meatCount: counts['meat'] || 0,
        vegeterianCount: counts['vegeterian'] || 0,
        veganCount: counts['vegan'] || 0,
        sweetCount: counts['sweet'] || 0,
        saltyCount: counts['salty'] || 0
      });
    } catch (err) {
      console.error('Error fetching preference counts:', err);
      res.status(500).json({ error: 'Failed to fetch counts' });
    }
  });

  function determineStyle(questions) {
    const styles = {
        meat: ['meat'],
        vegeterian: ['vegeterian'],
        vegan: ['vegan'],
        sweet: ['sweet'],
        salty: ['salty']
    }

    let meatCount = 0, vegeterianCount = 0, veganCount = 0, sweetCount = 0, saltyCount = 0

    questions.forEach(question => {
        if (styles.meat.includes(question)) meatCount++
        if (styles.vegeterian.includes(question)) vegeterianCount++
        if (styles.vegan.includes(question)) veganCount++
        if (styles.sweet.includes(question)) sweetCount++
        if (styles.salty.includes(question)) saltyCount++
    })

    if (meatCount > 0) return 'meat'
    if (vegeterianCount > 0) return 'vegeterian'
    if (veganCount > 0) return 'vegan'
    if (sweetCount > 0) return 'sweet'
    if (saltyCount > 0) return 'salty'
}



module.exports = users
