const express = require("express");
const ratings = express.Router();
const DB = require("../db/dbConn");


// DB VERSION ????
// Update rating for a recipe
// ratings.post("/rateRecipe", async (req, res) => {
//   const { recipeID, rating } = req.body;

//   if (!recipeID || !rating) {
//     return res.status(400).json({ success: false, message: "Recipe ID and rating are required" });
//   }

//   try {
//     // Fetch the current rating and count of ratings
//     const recipe = await DB.query(`SELECT rating FROM RECIPES WHERE recipeID = ?`, [recipeID]);

//     if (recipe.length === 0) {
//       return res.status(404).json({ success: false, message: "Recipe not found" });
//     }

//     const currentRating = parseFloat(recipe[0].rating) || 0; // Current average rating
//     const newRating = (currentRating + parseFloat(rating)) / 2; // Calculate new average

//     // Update the `rating` column in the RECIPES table
//     await DB.query(`UPDATE RECIPES SET rating = ? WHERE recipeID = ?`, [newRating.toFixed(1), recipeID]);

//     res.status(200).json({ success: true, message: "Rating updated successfully", newRating: newRating.toFixed(1) });
//   } catch (err) {
//     console.error("Error updating rating:", err);
//     res.status(500).json({ success: false, message: "Failed to update rating" });
//   }
// });

// // Get the average rating for a recipe
// ratings.get("/getRating/:recipeID", async (req, res) => {
//   const { recipeID } = req.params;

//   try {
//     const recipe = await DB.query(`SELECT rating FROM RECIPES WHERE recipeID = ?`, [recipeID]);

//     if (recipe.length === 0) {
//       return res.status(404).json({ success: false, message: "Recipe not found" });
//     }

//     res.status(200).json({ success: true, rating: recipe[0].rating });
//   } catch (err) {
//     console.error("Error fetching rating:", err);
//     res.status(500).json({ success: false, message: "Failed to fetch rating" });
//   }
// });

/// --------------------------------------------------
// API
// ratings.post("/rateRecipe", async (req, res) => {
//     const { title, rating } = req.body; // Use `title` for API-based recipes
  
//     if (!title || !rating) {
//       return res.status(400).json({ success: false, message: "Recipe title and rating are required" });
//     }
  
//     try {
//       // Fetch the recipeID using the title
//       const queryRecipe = `SELECT recipeID, rating FROM RECIPES WHERE title = ? LIMIT 1`;
//       const recipeResult = await DB.query(queryRecipe, [title]);
  
//       if (recipeResult.length === 0) {
//         return res.status(404).json({ success: false, message: "Recipe not found" });
//       }
  
//     //   const recipeID = recipeResult[0].recipeID;
//     //   const currentRating = parseFloat(recipeResult[0].rating) || 0; // Current average rating
//     //   const newRating = (currentRating + parseFloat(rating)) / 2; // Calculate new average
  
//       // Update the `rating` column in the RECIPES table
//       await DB.query(`UPDATE RECIPES SET rating = ? WHERE recipeID = ?`, [newRating.toFixed(1), recipeID]);
  
//       res.status(200).json({ success: true, message: "Rating updated successfully", newRating: newRating.toFixed(1) });
//     } catch (err) {
//       console.error("Error updating rating:", err);
//       res.status(500).json({ success: false, message: "Failed to update rating" });
//     }
//   });

ratings.post("/rateRecipe", async (req, res) => {
    try {
      console.log("Incoming request body:", req.body);
      const { title, rating } = req.body;
  
      if (!title || rating == null) {
        return res.status(400).json({ success: false, message: "Title and rating are required" });
      }
  
      // Parse and validate the rating
      const parsedRating = parseInt(rating, 10); // Convert to integer
      if (isNaN(parsedRating) || parsedRating < 1 || parsedRating > 5) {
        return res.status(400).json({ success: false, message: "Rating must be an integer between 1 and 5" });
      }
  
      // Update the database
      const query = `UPDATE RECIPES SET rating = ? WHERE title = ?`;
      const result = await DB.query(query, [parsedRating, title]);
  
      if (result.affectedRows === 0) {
        return res.status(404).json({ success: false, message: "Recipe not found" });
      }
  
      res.status(200).json({ success: true, message: "Rating updated successfully" });
    } catch (err) {
      console.error("Error updating recipe rating:", err);
      res.status(500).json({ success: false, message: "Failed to update recipe rating" });
    }
  });
  

module.exports = ratings;
