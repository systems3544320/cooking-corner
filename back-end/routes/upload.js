const multer = require('multer');
const express = require("express")
const upload = express.Router();
const DB = require('../db/dbConn.js')


const storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, 'uploads')
    },
    filename: (req, file, callBack) => {
        callBack(null, `${file.originalname}`)
    }
  })
  
// Configure Multer,
let upload_dest = multer({ dest: 'uploads/' })

//upload_dest.single('file') => callback that should be called once this happens.
//upload_dest.array('files') => callback that should be called once this happens.
upload.post('/', upload_dest.single('file'), async (req, res, next) => {
    const file = req.file;
    console.log(file.filename);
    if (!file) {
      res.send({ status: { success: false, msg: "Could not uplpad" }});
    }else{
      res.send({ status: { success: true, msg: "File upladed" }});
    }   
})

upload.post('/saveRecipe', async (req, res, next) => {
  const { title, cookingTime, image, mealType, calories, ingredients } = req.body;

  try {
      // Insert recipe into the RECIPES table
      const recipeResult = await DB.query(
          `INSERT INTO RECIPES (title, cookingTime, image, mealType, calories) VALUES (?, ?, ?, ?, ?)`,
          [title, cookingTime, image, mealType, calories]
      );
      const recipeID = recipeResult.insertId;

      // Insert ingredients into the INGREDIENT table
      for (const ingredient of ingredients) {
          await DB.query(
              `INSERT INTO INGREDIENT (name, quantity, measure, r_recipeID) VALUES (?, ?, ?, ?)`,
              [ingredient.name, ingredient.quantity, ingredient.measure, recipeID]
          );
      }

      res.status(200).send("Recipe and ingredients saved successfully");
  } catch (err) {
      console.error("Error saving recipe:", err);
      res.status(500).send("Failed to save recipe");
  }
});


module.exports = upload

