const express = require("express");
const shoppingList = express.Router();
const DB = require("../db/dbConn");

// Add ingredients to the shopping list
shoppingList.post("/addToShoppingList", async (req, res) => {
    const { ingredients } = req.body;

    if (!ingredients || !Array.isArray(ingredients)) {
        return res.status(400).send("Invalid ingredients list");
    }

    try {
        for (const ingredient of ingredients) {
            await DB.query(
                `INSERT INTO SHOPPING_LIST (ingredientName, quantity, measure) VALUES (?, ?, ?)`,
                [ingredient.food, ingredient.quantity, ingredient.measure]  // API
                // [ingredient.name, ingredient.quantity, ingredient.measure]  // DB
            );
        }
        res.status(200).send("Ingredients added to shopping list");
    } catch (err) {
        console.error("Error adding to shopping list:", err);
        res.status(500).send("Failed to add to shopping list");
    }
});

// Fetch shopping list
shoppingList.get("/fetchShoppingList", async (req, res) => {
    try {
        const shoppingList = await DB.query(`SELECT * FROM SHOPPING_LIST`);
        res.json(shoppingList);
    } catch (err) {
        console.error("Error fetching shopping list:", err);
        res.status(500).send("Failed to fetch shopping list");
    }
});

// Update shopping list item (e.g., mark as purchased)
shoppingList.put("/updateShoppingList", async (req, res) => {
    const { id, isPurchased } = req.body;

    if (!id) {
        return res.status(400).send("Item ID is required");
    }

    try {
        await DB.query(
            `UPDATE SHOPPING_LIST SET isPurchased = ? WHERE id = ?`,
            [isPurchased, id]
        );
        res.status(200).send("Shopping list item updated");
    } catch (err) {
        console.error("Error updating shopping list:", err);
        res.status(500).send("Failed to update shopping list");
    }
});

module.exports = shoppingList;
