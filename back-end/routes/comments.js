const express = require("express");
const comments = express.Router();
const DB = require("../db/dbConn");

// Add a new comment ------ DB version
// comments.post("/addComment", async (req, res) => {
//   console.log("Request body:", req.body); 
//   const { r_recipeID, text } = req.body;

//   if (!r_recipeID || !text) {
//     return res.status(400).json({ success: false, message: "Recipe ID and text are required" });
//   }

//   try {
//     const query = `INSERT INTO COMMENT (r_recipeID, text, date) VALUES (?, ?, CURRENT_TIMESTAMP)`;
//     const result = await DB.query(query, [r_recipeID, text]);
//     res.status(200).json({ success: true, message: "Comment added successfully", commentID: result.insertId });
//   } catch (err) {
//     console.error("Error adding comment:", err);
//     res.status(500).json({ success: false, message: "Failed to add comment" });
//   }
// });

// Fetch comments for a specific recipe   DB
// comments.get("/getComments/:recipeID", async (req, res) => {
//   const { recipeID } = req.params;

//   try {
//     const query = `SELECT * FROM COMMENT WHERE r_recipeID = ? ORDER BY date DESC`;
//     const comments = await DB.query(query, [recipeID]);
//     res.status(200).json(comments);
//   } catch (err) {
//     console.error("Error fetching comments:", err);
//     res.status(500).json({ success: false, message: "Failed to fetch comments" });
//   }
// });

// --------------------------------

// add new comment ---- API version ----
comments.post("/addComment", async (req, res) => {
  const { title, text } = req.body;

  if (!title || !text) {
    return res.status(400).json({ success: false, message: "Recipe title and text are required" });
  }

  try {
    // Fetch the recipeID using the title
    const queryRecipe = `SELECT recipeID FROM RECIPES WHERE title = ? LIMIT 1`;
    const recipeResult = await DB.query(queryRecipe, [title]);

    if (recipeResult.length === 0) {
      return res.status(404).json({ success: false, message: "Recipe not found" });
    }

    const recipeID = recipeResult[0].recipeID;

    // Insert the comment
    const query = `INSERT INTO COMMENT (r_recipeID, text, date) VALUES (?, ?, CURRENT_TIMESTAMP)`;
    const result = await DB.query(query, [recipeID, text]);

    res.status(200).json({ success: true, message: "Comment added successfully", commentID: result.insertId });
  } catch (err) {
    console.error("Error adding comment:", err);
    res.status(500).json({ success: false, message: "Failed to add comment" });
  }
});

comments.get("/getComments/:title", async (req, res) => {
  const { title } = req.params;

  try {
    // Fetch the recipeID using the title
    const queryRecipe = `SELECT recipeID FROM RECIPES WHERE title = ? LIMIT 1`;
    const recipeResult = await DB.query(queryRecipe, [title]);

    if (recipeResult.length === 0) {
      return res.status(404).json({ success: false, message: "Recipe not found" });
    }

    const recipeID = recipeResult[0].recipeID;

    // Fetch comments using the recipeID
    const query = `SELECT * FROM COMMENT WHERE r_recipeID = ? ORDER BY date DESC`;
    const comments = await DB.query(query, [recipeID]);

    res.status(200).json(comments);
  } catch (err) {
    console.error("Error fetching comments:", err);
    res.status(500).json({ success: false, message: "Failed to fetch comments" });
  }
});




module.exports = comments;
