const mysql = require('mysql2');

const conn = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS, 
    database: process.env.DB_DATABASE,
  })

 conn.connect((err) => {
      if(err){
          console.log("ERROR: " + err.message);
          return;    
      }
      console.log('Connection established');
    })


    let dataPool={}
  
dataPool.allNovice=()=>{
  return new Promise ((resolve, reject)=>{
    conn.query(`SELECT * FROM news`, (err,res)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}

dataPool.oneNovica=(id)=>{
  return new Promise ((resolve, reject)=>{
    conn.query(`SELECT * FROM news WHERE id = ?`, id, (err,res)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}

dataPool.creteNovica=(title,slug,text,file)=>{
  return new Promise ((resolve, reject)=>{
    conn.query(`INSERT INTO news (title,slug,text, file) VALUES (?,?,?,?)`, [title, slug, text, file], (err,res)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}

dataPool.AuthUser=(username, password)=>
{
  return new Promise ((resolve, reject)=>{
    conn.query('SELECT * FROM USER WHERE username = ? ', username, (err,res, fields)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })  
	
}

dataPool.AddUser=(username,email,password, userType)=>{  
  return new Promise ((resolve, reject)=>{   
    conn.query(`INSERT INTO USER (username,email,password,userType) VALUES (?,?,?,?)`, [username, email, password, userType], (err,res)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}

dataPool.userProfile=(userID, username, email, password)=>{
  return new Promise ((resolve, reject)=>{
    conn.query(`SELECT * FROM USER WHERE (userID, username,email, password) VALUES (?,?,?,?)`, [userID, username, email, password], (err,res)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}

dataPool.searchIngredient=(name) => {
  return new Promise ((resolve, reject)=>{   
    conn.query(`SELECT * FROM INGREDIENT WHERE name = ?`, [name], (err,res)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}

dataPool.AddPreferences=(user_input) => {
  return new Promise ((resolve, reject) =>{
    const query = 'INSERT INTO PREFERENCES (user_input) VALUES (?)'
      conn.query(query, [user_input], (err,res) =>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}

dataPool.GetPreferences = () => {
  return new Promise((resolve, reject) => {
    const query = 'SELECT user_input, COUNT(*) AS count FROM PREFERENCES GROUP BY user_input';
    conn.query(query, (err, res) => {
      if (err) {
        return reject(err);
      }
      return resolve(res);
    });
  });
};

// like dislike page preferences
// Add a page preference (like/dislike)
dataPool.AddPagePreference = (user_input) => {
  return new Promise((resolve, reject) => {
    const query = 'INSERT INTO PREFERENCES (user_input) VALUES (?)';
    conn.query(query, [user_input], (err, res) => {
      if (err) {
        return reject(err);
      }
      return resolve(res);
    });
  });
};

// Get the count of likes and dislikes for the page
dataPool.GetPagePreferenceCount = () => {
  return new Promise((resolve, reject) => {
    const query = 'SELECT user_input, COUNT(*) AS count FROM PREFERENCES WHERE user_input IN ("like", "dislike") GROUP BY user_input';
    conn.query(query, (err, res) => {
      if (err) {
        return reject(err);
      }
      return resolve(res);
    });
  });
};


dataPool.saveRecipe = (recipe) => {
  return new Promise((resolve, reject) => {
    conn.query(
      `INSERT INTO RECIPES (title, calories, image, cookingTime, mealType) VALUES (?, ?, ?, ?, ?)`,
      [recipe.title, recipe.calories, recipe.image, recipe.totalTime, recipe.mealType],
      (err, res) => {
        if (err) return reject(err);

        // Fetch the correct recipeID after the insert
        conn.query(
          `SELECT recipeID FROM RECIPES WHERE title = ? ORDER BY recipeID DESC LIMIT 1`,
          [recipe.title],
          (err, results) => {
            if (err) return reject(err);
            resolve(results[0].recipeID); // Return the correct recipeID
          }
        );
      }
    );
  });
};



dataPool.saveIngredient = (ingredient, recipeID) => {
  // console.log("Saving ingredient:", ingredient.name, "with recipeID:", recipeID); // Debug log
  return new Promise((resolve, reject) => {
    conn.query(
      `INSERT INTO INGREDIENT (name, quantity, measure, r_recipeID) VALUES (?, ?, ?, ?)`,
      [ingredient.name, ingredient.quantity, ingredient.measure, recipeID], // Use the recipeID here
      (err, res) => {
        if (err) {
          console.error("Error saving ingredient:", err);
          return reject(err);
        }
        resolve(res);
      }
    );
  });
};


dataPool.query = (query, params) => {
  return new Promise((resolve, reject) => {
      conn.query(query, params, (err, results) => {
          if (err) return reject(err);
          resolve(results);
      });
  });
};





module.exports = dataPool;

