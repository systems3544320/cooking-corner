import React from "react";
import axios from "axios";
import { API_URL } from "../Utils/Configuration";

class MealplanView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mealPlanRecipes: [],
            error: null
        };
    }

    componentDidMount() {
        this.fetchMealPlan(); // Fetch meal plan recipes when component mounts
    }

    fetchMealPlan = () => {
        axios.get(`${API_URL}/mealplan/fetchMealPlan`, { withCredentials: true })
            .then((response) => {
                console.log("Meal Plan Data Fetched:", response.data);
                const groupedByDay = response.data.reduce((acc, recipe) => {
                    const day = recipe.dayOfWeek || "Unassigned";
                    if (!acc[day]) acc[day] = [];
                    acc[day].push(recipe);
                    return acc;
                }, {});
                // this.setState({ mealPlanRecipe: groupedByDay });
                // console.log("Fetched meal plan recipes:", response.data);
                this.setState({ mealPlanRecipes: response.data, error: null });
            })
            .catch((err) => {
                console.error("Error fetching meal plan recipes:", err);
                this.setState({ error: "Failed to fetch meal plan recipes" });
            });
    };

    // removeFromMealPlan = (planID) => {
    //     axios
    //         .delete(`${API_URL}/mealplan/removeFromMealPlan`, {
    //             data: { planID }, // Send planID as part of the request
    //             withCredentials: true,
    //         })
    //         .then((response) => {
    //             console.log("Recipe removed from meal plan:", response.data);
    //             // Update the meal plan by removing the deleted recipe
    //             this.setState((prevState) => ({
    //                 mealPlanRecipes: prevState.mealPlanRecipes.filter((recipe) => recipe.planID !== planID),
    //             }));
    //         })
    //         .catch((err) => {
    //             console.error("Error removing recipe from meal plan:", err);
    //             alert("Failed to remove recipe from meal plan");
    //         });
    // };

    removeFromMealPlan = (recipeID) => {
        console.log('Recipe ID to remove:', recipeID); // Debugging
    
        axios
            .delete(`${API_URL}/mealplan/removeFromMealPlan`, {
                data: { recipeID }, // Pass recipeID as data
                withCredentials: true,
            })
            .then((response) => {
                console.log('Removed from meal plan:', response.data);
                alert('Recipe removed from meal plan!');
                this.fetchMealPlan(); // Refresh the meal plan
            })
            .catch((err) => {
                console.error('Error removing recipe from meal plan:', err);
                alert('Failed to remove recipe from meal plan.');
            });
    };

    // handleDayChange = (planID, dayOfWeek) => {
    //     axios
    //         .put(`${API_URL}/mealplan/updateDay`, { planID, dayOfWeek })
    //         .then((response) => {
    //             alert("Day updated successfully!");
    //             this.fetchMealPlan(); // Refresh the meal plan
    //         })
    //         .catch((err) => {
    //             console.error("Error updating day:", err);
    //             alert("Failed to update day.");
    //         });
    // };
    
    handleRemove = (recipeID) => {
        if (!window.confirm("Are you sure you want to remove this recipe from your meal plan?")) {
            return;
        }
    
        axios
            .delete(`${API_URL}/mealplan/removeFromMealPlan`, {
                data: { recipeID }, // Pass recipeID in the request body
                withCredentials: true,
            })
            .then((response) => {
                console.log("Recipe removed from meal plan:", response.data);
                this.fetchMealPlan(); // Refresh the meal plan after deletion
            })
            .catch((err) => {
                console.error("Error removing recipe from meal plan:", err);
                alert("Failed to remove recipe from meal plan");
            });
    };
    
    

    render() {
        const { mealPlanRecipes, error } = this.state;

        return (
            <div className="card" style={{ margin: "10px" }}>
                <div className="card-body">
                    <h5 className="card-title">Your Meal Plan</h5>
                    {error && <p style={{ color: "red" }}>{error}</p>}

                    <div>
                        {mealPlanRecipes.length > 0 ? (
                            mealPlanRecipes.map((recipe) => (
                                <div
                                    key={recipe.planID}
                                    style={{
                                        border: "1px solid #ccc",
                                        padding: "10px",
                                        margin: "10px 0",
                                    }}
                                >
                                    <h3>{recipe.title}</h3>
                                    <p>Meal Type: {recipe.mealType}</p>
                                    <p>Cooking Time: {recipe.cookingTime} minutes</p>
                                    <p>Calories: {recipe.calories}</p>
                                    <p>Date Added: {new Date(recipe.date).toLocaleDateString()}</p>
                                    <img src={recipe.image} alt={recipe.title} width="200" />
                                    <button
                                        className="btn btn-danger"
                                        onClick={() => this.handleRemove(recipe.recipeID)}
                                    >
                                        Remove from Meal Plan
                                    </button>
                                    <p>
                                Recipe ID: {recipe.r_recipeid}, Planned for:{" "}
                                {recipe.dayOfWeek || "Not Set"}
                                 </p>
                             </div>
                            ))
                        ) : (
                            <p>No recipes in your meal plan</p>
                        )}

                        {/* <h5 className="card-title">Meal Plan</h5>
                        {mealPlanRecipes.map((day) => (
                            <div key={day}>
                                <h6>{day}</h6>
                                <ul>
                                    {mealPlanRecipes[day].map((recipe) => (
                                        <li key={recipe.planID}>
                                            <p><strong>{recipe.title}</strong></p>
                                            <p>Cooking Time: {recipe.cookingTime} minutes</p>
                                            <p>Calories: {recipe.calories}</p>
                                            <p>Meal Type: {recipe.mealType}</p>
                                            <img src={recipe.image} alt={recipe.title} style={{ width: "150px" }} />
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        ))} */}

                    </div>
                </div>
            </div>
        );
    }

}

export default MealplanView