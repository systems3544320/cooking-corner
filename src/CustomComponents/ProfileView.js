import React from "react";
import axios from "axios";
import { API_URL } from "../Utils/Configuration";

class ProfileView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_input: "",
            meatCount: 0,
            vegeterianCount: 0,
            veganCount: 0,
            sweetCount: 0,
            saltyCount: 0,
            status: {
              success: false,
              msg: ""
            }
          }
      }

    // QGetTextFromField = (e) => {
    //     this.setState(this.state.user_input[e.target.name] = [e.target.value])
    //     console.log(this.state)
    // }
    handleOptionChange = (e) => {
        this.setState({ user_input: e.target.value });
      }

    fetchPreferencesCount = () => {
        axios.get(`${API_URL}/users/count`, { withCredentials: true })
          .then(response => {
            console.log("API Response:", response.data);  
            const { meatCount, vegeterianCount, veganCount, sweetCount, saltyCount } = response.data;
            this.setState({ meatCount, vegeterianCount, veganCount, sweetCount, saltyCount });
          })
          .catch(err => {
            console.error("Error fetching preferences count:", err);
          });
      }
      componentDidMount() {
        this.fetchPreferencesCount();
      }

    submitPreferences = (event) => {
        event.preventDefault();
    
        axios.post(`${API_URL}/users/preferences`, {
            user_input: this.state.user_input
        }, { withCredentials: true })
        .then(response => {
          if (response.data.success) {
            //alert("Preference saved successfully!");
            this.fetchPreferencesCount(); 
          } else {
            alert("Failed to save preferences.");
          }
        })
        .catch(err => {
          console.error("Error submitting preferences:", err);
        });
      }

    render(){
        const { meatCount, vegeterianCount, veganCount, sweetCount, saltyCount } = this.state;

        return(
          <div className="card" style={{margin:"10px"}}>
            <div className="card-body">
                <h5 className="card-title">Welcome!!!</h5>
                {/* <p className="card-text">You are in profile</p> */}
                <label>Please select your preferences: </label>
                {/* <div>
                    <label>
                    <input type="checkbox" name="foodPreference" value="meat" onClick={(e) => this.QGetTextFromField(e)} />
                    Meat
                    </label>
                </div>
                <div>
                    <label>
                    <input type="checkbox" name="foodPreference" value="vegeterian" onClick={(e) => this.QGetTextFromField(e)} />
                    Vegeterian
                    </label>
                </div>
                <div>
                    <label>
                    <input type="checkbox" name="foodPreference" value="vegan" onClick={(e) => this.QGetTextFromField(e)} />
                    Vegan
                    </label>
                </div>
                <div>
                    <h3>Currently popular:</h3>
                    <p>Meat: {meatCount} people</p>
                    <p>Vegetarien: {vegeterianCount} people</p>
                </div> */}

                <div>
                    <form onSubmit={this.submitPreferences}>
                    <label>
                    <input 
                        type="radio" 
                        value="meat" 
                        checked={this.state.user_input === 'meat'} 
                        onChange={this.handleOptionChange} 
                    />
                    MEAT
                    </label>
                    <br></br>
                    <label>
                    <input 
                        type="radio" 
                        value="vegeterian" 
                        checked={this.state.user_input === 'vegeterian'} 
                        onChange={this.handleOptionChange} 
                    />
                    VEGETERIAN
                    </label>
                    <br></br>
                    <label>
                    <input 
                        type="radio" 
                        value="vegan" 
                        checked={this.state.user_input === 'vegan'} 
                        onChange={this.handleOptionChange} 
                    />
                    VEGAN
                    </label>
                    <br></br>
                    <button style={{ margin: "10px" }} className="btn btn-primary bt" type="submit">Save Preferences</button>
                    </form>
                </div>
                <div>
                    <h3>Preference counter:</h3>
                    <p>Meat: {meatCount} people</p>
                    <p>Vegetarien: {vegeterianCount} people</p>
                    <p>Vegan: {veganCount} people</p>
                </div>

                {/* line break */}
                <hr></hr>
                <label>What do you like more? </label>
                {/* <div>
                    <label>
                    <input type="checkbox" name="preference" value="salty" onClick={(e) => this.QGetTextFromField(e)} />
                    Salty food
                    </label>
                </div>
                <div>
                    <label>
                    <input type="checkbox" name="preference" value="sweet" onClick={(e) => this.QGetTextFromField(e)} />
                    Sweet food
                    </label>
                </div> */}
                <div>
                    <form onSubmit={this.submitPreferences}>
                    <label>
                    <input 
                        type="radio" 
                        value="sweet" 
                        checked={this.state.user_input === 'sweet'} 
                        onChange={this.handleOptionChange} 
                    />
                    SWEETS
                    </label>
                    <br></br>
                    <label>
                    <input 
                        type="radio" 
                        value="salty" 
                        checked={this.state.user_input === 'salty'} 
                        onChange={this.handleOptionChange} 
                    />
                    SALTY
                    </label>
                    <br></br>
                    <button style={{ margin: "10px" }} className="btn btn-primary bt" type="submit">Save Your Answer</button>
                    </form>
                </div>
                <div>
                    <h3>Preference counter:</h3>
                    <p>Sweet: {sweetCount} people</p>
                    <p>Salty: {saltyCount} people</p>
                </div>
            </div>
        </div>
        )
    }

}

export default ProfileView