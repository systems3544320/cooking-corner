// import React, { Component } from "react";
// import axios from "axios";

// import { API_URL } from "../Utils/Configuration";


// class FilesUpload extends Component {
  
//   // differnt types of encoding of sending from data. Deafualt is application/x-www-form-urlencoded 
//   // for sending files you need to do multipart/form-data
//   uploadFile(event){
//     const data = new FormData() ;
//     data.append('file', event.target.files[0]);
//     axios.post(API_URL+"/uploadFile", data)
//         .then(res => { // then print response status
//           console.log(res.data)
//         })
//   }

//   render() {
//     return (

//       <div className="card"
//         style={{ margin: "10px" }}>
//         <h3 style={{ margin: "10px" }}>File upload</h3>
//         <div className="mb-3"
//           style={{ margin: "10px" }}>
//               <div className="mb-3">
//                 <label for="formFile" class="form-label">Default file input example</label>
//                 <input class="form-control" type="file" id="file" onChange={(e) => {this.uploadFile(e)}} />
//               </div>
//           </div>
//       </div>
//     );
//   }
// }

// export default FilesUpload


import React, { Component } from "react";
import axios from "axios";
import { API_URL } from "../Utils/Configuration";

class FilesUpload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      cookingTime: "",
      image: "",
      mealType: "",
      calories: "",
      ingredients: [{ name: "", quantity: "", measure: "" }], // Initial ingredient
      successMessage: "",
      errorMessage: ""
    };
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleIngredientChange = (index, e) => {
    const ingredients = [...this.state.ingredients];
    ingredients[index][e.target.name] = e.target.value;
    this.setState({ ingredients });
  };

  addIngredient = () => {
    this.setState((prevState) => ({
      ingredients: [...prevState.ingredients, { name: "", quantity: "", measure: "" }]
    }));
  };

  removeIngredient = (index) => {
    this.setState((prevState) => ({
      ingredients: prevState.ingredients.filter((_, i) => i !== index)
    }));
  };

  saveRecipe = () => {
    const { title, cookingTime, image, mealType, calories, ingredients } = this.state;

    axios
      .post(`${API_URL}/upload/saveRecipe`, {
        title,
        cookingTime,
        image,
        mealType,
        calories,
        ingredients
      }, { withCredentials: true })
      .then((res) => {
        this.setState({ successMessage: "Recipe saved successfully!", errorMessage: "" });
      })
      .catch((err) => {
        this.setState({ errorMessage: "Failed to save recipe", successMessage: "" });
      });
  };

  render() {
    const { title, cookingTime, image, mealType, calories, ingredients, successMessage, errorMessage } = this.state;

    return (
      <div className="card" style={{ margin: "10px" }}>
        <h3 style={{ margin: "10px" }}>Add New Recipe</h3>
        <div style={{ margin: "10px" }}>
          <div className="mb-3">
            <label>Title</label>
            <input
              type="text"
              className="form-control"
              name="title"
              value={title}
              onChange={this.handleChange}
            />
          </div>
          <div className="mb-3">
            <label>Cooking Time (in minutes)</label>
            <input
              type="number"
              className="form-control"
              name="cookingTime"
              value={cookingTime}
              onChange={this.handleChange}
            />
          </div>
          <div className="mb-3">
            <label>Image URL</label>
            <input
              type="text"
              className="form-control"
              name="image"
              value={image}
              onChange={this.handleChange}
            />
          </div>
          <div className="mb-3">
            <label>Meal Type</label>
            <input
              type="text"
              className="form-control"
              name="mealType"
              value={mealType}
              onChange={this.handleChange}
            />
          </div>
          <div className="mb-3">
            <label>Calories</label>
            <input
              type="number"
              className="form-control"
              name="calories"
              value={calories}
              onChange={this.handleChange}
            />
          </div>
          <div className="mb-3">
            <h5>Ingredients</h5>
            {ingredients.map((ingredient, index) => (
              <div key={index} style={{ marginBottom: "10px" }}>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Ingredient Name"
                  name="name"
                  value={ingredient.name}
                  onChange={(e) => this.handleIngredientChange(index, e)}
                  style={{ display: "inline-block", width: "30%", marginRight: "10px" }}
                />
                <input
                  type="number"
                  className="form-control"
                  placeholder="Quantity"
                  name="quantity"
                  value={ingredient.quantity}
                  onChange={(e) => this.handleIngredientChange(index, e)}
                  style={{ display: "inline-block", width: "30%", marginRight: "10px" }}
                />
                <input
                  type="text"
                  className="form-control"
                  placeholder="Measure"
                  name="measure"
                  value={ingredient.measure}
                  onChange={(e) => this.handleIngredientChange(index, e)}
                  style={{ display: "inline-block", width: "30%", marginRight: "10px" }}
                />
                <button
                  className="btn btn-danger"
                  onClick={() => this.removeIngredient(index)}
                  style={{ display: "inline-block" }}
                >
                  Remove
                </button>
              </div>
            ))}
            <button className="btn btn-primary" onClick={this.addIngredient}>
              Add Ingredient
            </button>
          </div>
          <button className="btn btn-success" onClick={this.saveRecipe}>
            Save Recipe
          </button>
          {successMessage && <p style={{ color: "green", marginTop: "10px" }}>{successMessage}</p>}
          {errorMessage && <p style={{ color: "red", marginTop: "10px" }}>{errorMessage}</p>}
        </div>
      </div>
    );
  }
}

export default FilesUpload;
