import React from "react";
import axios from "axios";
import { API_URL } from "../Utils/Configuration";

// class ShoppingListView extends React.Component {
//     render(){
//         return(
//           <div className="card" style={{margin:"10px"}}>
//             <div className="card-body">
//                 <h5 className="card-title">Welcome!!!</h5>
//                 <p className="card-text">You are in shopping list</p>
//             </div>
//         </div>
//         )
//     }

// }

class ShoppingListView extends React.Component {
    state = {
        shoppingList: [],
    };

    componentDidMount() {
        this.fetchShoppingList();
    }

    fetchShoppingList = () => {
        axios
            .get(`${API_URL}/shoppinglist/fetchShoppingList`)
            .then((response) => {
                this.setState({ shoppingList: response.data });
            })
            .catch((err) => {
                console.error("Error fetching shopping list:", err);
            });
    };

    handlePurchaseChange = (id, isPurchased) => {
        axios
            .put(`${API_URL}/shoppinglist/updateShoppingList`, {
                id,
                isPurchased,
            })
            .then(() => {
                this.fetchShoppingList();
            })
            .catch((err) => {
                console.error("Error updating shopping list:", err);
            });
    };

    render() {
        const { shoppingList } = this.state;

        return (
            <div className="card" style={{ margin: "10px" }}>
                <div className="card-body">
                    <h5 className="card-title">Shopping List</h5>
                    <ul>
                        {shoppingList.map((item) => (
                            <li key={item.id}>
                                <input
                                    type="checkbox"
                                    checked={item.isPurchased}
                                    onChange={(e) =>
                                        this.handlePurchaseChange(
                                            item.id,
                                            e.target.checked
                                        )
                                    }
                                />
                                {item.ingredientName} {item.quantity} {item.measure}
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        );
    }
}


export default ShoppingListView