import React, {  Component  } from "react";
import axios from "axios";
import { API_URL } from "../Utils/Configuration";
import { FaStar } from "react-icons/fa";
  
// const APIurl = axios.get(`${API_URL}/search/fetchRecipes`, { withCredentials: true });

class SearchView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: "",
      recipes: [],
      results: [],
      ingredients: [],
      selectedIngredients: [],
      comments: {}, // Store comments for each recipe
      newComment: "", // New comment text
      error: null
    };
  }

  handleInputChange = (e) => {
    this.setState({ searchTerm: e.target.value });
  };
  
    // Fetch recipes from the backend API !!!!!!!!!!
    fetchRecipes = () => {
      axios
        .get(`${API_URL}/search/fetchRecipes`, { withCredentials: true })
        .then((response) => {
          console.log("Fetched Recipes:", response.data); // Debugging the response

          const recipes = response.data;

          // Fetch comments for each recipe
          recipes.forEach((recipe) => {
            this.fetchComments(recipe.recipe.label || recipe.title);
          });

          this.setState({ recipes: response.data }); // Store recipes in state
        })
        .catch((err) => {
          console.error("Error fetching recipes:", err);
          this.setState({ error: "Failed to fetch recipes" }); // Store error
        });
    };

    // Fetch recipes and ingredients from the backend FROM DATABASE
  //   fetchRecipesAndIngredients = async () => {
  //     try {
  //         // Fetch recipes
  //         const recipesResponse = await axios.get(`${API_URL}/search/fetchRecipes`, { withCredentials: true });
  //         const recipes = recipesResponse.data;

  //         // Fetch ingredients
  //         const ingredientsResponse = await axios.get(`${API_URL}/search/fetchIngredients`, { withCredentials: true });
  //         const ingredients = ingredientsResponse.data;

  //         // Combine ingredients with recipes
  //         const combinedRecipes = recipes.map((recipe) => {
  //             return {
  //                 ...recipe,
  //                 ingredients: ingredients.filter((ingredient) => ingredient.r_recipeID === recipe.recipeID),
  //             };
  //         });

  //         // Fetch comments for each recipe
  //         // combinedRecipes.forEach((recipe) => this.fetchComments(recipe.recipeID));
  //         this.setState({ recipes: combinedRecipes, error: null });
  //     } catch (err) {
  //         console.error("Error fetching recipes or ingredients:", err);
  //         this.setState({ error: "Failed to fetch recipes or ingredients" });
  //     }
  // };

  saveToMealPlan = (recipeTitle) => {
    const userID = 1; // Replace with dynamic user ID if available
    const date = new Date().toISOString().split("T")[0]; // Current date in YYYY-MM-DD format
    const dayOfWeek = this.state[`day_${recipeTitle}`] || ""; // Selected day of the week

    if (!dayOfWeek) {
        alert("Please select a day of the week.");
        return;
    }

    console.log("Saving recipe to meal plan with title:", recipeTitle, "and day:", dayOfWeek);

    axios
        .post(
            `${API_URL}/search/saveToMealPlan`,
            { userID, recipeTitle, date, dayOfWeek },
            { withCredentials: true }
        )
        .then((response) => {
            console.log("Recipe saved to meal plan:", response.data);
            alert("Recipe added to your meal plan!");
        })
        .catch((err) => {
            console.error("Error saving to meal plan:", err);
            alert("Failed to add recipe to meal plan.");
        });
  };




    handleSearch = () => {
      axios
        .get(`${API_URL}/search/search`, { params: { query: this.state.searchTerm }, withCredentials: true })
        .then((response) => {
          // setResults(response.data);
          console.log("Fetched Recipes:", response.data); // Debugging the response
          this.setState({ results: response.data || [], error: null }); // Store recipes in state
        })
        .catch((err) => {
          console.error("Error fetching recipes:", err);
          this.setState({ error: "Failed to fetch recipes", results: [] }); // Store error
        });
    };

    // Handle Enter key for search
  handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.handleSearch();
    }
  };
  
    componentDidMount() {
      this.fetchRecipes(); // Fetch recipes when the component mounts
      // or fetchRecipes if fetchuing from API
      // fetchRecipesAndIngredients
    }


    // for shopping list
  handleCheckboxChange = (e, ingredient) => {
      const { selectedIngredients } = this.state;
  
      if (e.target.checked) {
          this.setState({
              selectedIngredients: [...selectedIngredients, ingredient],
          });
      } else {
          this.setState({
              selectedIngredients: selectedIngredients.filter(
                  // (item) => item.food !== ingredient.food  // API
                  (item) => item.ingredientName !== ingredient.name  // DB
              ),
          });
      }
  };
  
  saveIngredientsToShoppingList = () => {
      const { selectedIngredients } = this.state;
  
      axios
          .post(`${API_URL}/shoppinglist/addToShoppingList`, {
              ingredients: selectedIngredients,
          })
          .then((response) => {
              alert("Ingredients added to shopping list");
              console.log(response.data);
          })
          .catch((err) => {
              console.error("Error adding to shopping list:", err);
              alert("Failed to add ingredients to shopping list");
          });
  };

  // for DB comments
  // Fetch comments for a recipe
// fetchComments = async (recipeID) => {
//   try {
//     const response = await axios.get(`${API_URL}/comments/getComments/${recipeID}`, {
//       withCredentials: true,
//     });
//     this.setState((prevState) => ({
//       comments: { ...prevState.comments, [recipeID]: response.data },
//     }));
//   } catch (err) {
//     console.error("Error fetching comments:", err);
//   }
// };

// FETCH COMMENT API VERSION
fetchComments = async (recipeTitle) => {
  try {
    const response = await axios.get(`${API_URL}/comments/getComments/${recipeTitle}`, {
      withCredentials: true,
    });
    console.log(`Fetched comments for ${recipeTitle}:`, response.data); // Debugging

    this.setState((prevState) => ({
      comments: { ...prevState.comments, [recipeTitle]: response.data },
    }));
  } catch (err) {
    console.error("Error fetching comments:", err);
  }
};


// Handle new comment submission ----- DB
// handleAddComment = async (recipeID) => {
//   console.log("Adding comment for Recipe ID:", recipeID);

//   const { newComment } = this.state;

//   if (!newComment.trim()) {
//     alert("Comment cannot be empty!");
//     return;
//   }

//   try {
//     await axios.post(
//       `${API_URL}/comments/addComment`,
//       { r_recipeID: recipeID, text: newComment },
//       { withCredentials: true }
//     );
//     this.setState({ newComment: "" });
//     this.fetchComments(recipeID); // Refresh comments after adding
//   } catch (err) {
//     console.error("Error adding comment:", err);
//     alert("Failed to add comment.");
//   }
// };

// COMMET FOR API ---
handleAddComment = async (recipeTitle) => {
  console.log("Adding comment for Recipe ID:", recipeTitle);
  const { newComment } = this.state;

  if (!newComment.trim()) {
    alert("Comment cannot be empty!");
    return;
  }

  try {
    await axios.post(
      `${API_URL}/comments/addComment`,
      { title: recipeTitle, text: newComment }, // Send the title instead of recipeID
      { withCredentials: true }
    );
    this.setState({ newComment: "" });
    this.fetchComments(recipeTitle); // Update this to use title if needed
  } catch (err) {
    console.error("Error adding comment:", err);
    alert("Failed to add comment.");
  }
};


// HANDLE RATING -- DB VERSION
// handleRating = (recipeID, rating) => {
//   axios
//     .post(`${API_URL}/ratings/rateRecipe`, { recipeID, rating })
//     .then((response) => {
//       console.log("Rating updated:", response.data);
//       this.fetchRecipes(); // Refresh recipes to get updated ratings
//     })
//     .catch((err) => {
//       console.error("Error updating rating:", err);
//       alert("Failed to submit rating.");
//     });
// };

// HANDLE RATING --- API VERSION
// handleRating = (recipeTitle, rating) => {
//   axios
//     .post(`${API_URL}/ratings/rateRecipe`, { title: recipeTitle, rating: Math.round(rating) }) // Send `title` instead of `recipeID`
//     .then((response) => {
//       console.log("Rating updated:", response.data);
//       this.fetchRecipes(); // Refresh recipes to get updated ratings
//     })
//     .catch((err) => {
//       console.error("Error updating rating:", err);
//       alert("Failed to submit rating.");
//     });
// };

handleRating = async (title, rating) => {
  console.log("Sending rating:", { title, rating }); // Debug payload
  try {
    // Ensure the rating is an integer
    const response = await axios.post(
      `${API_URL}/ratings/rateRecipe`,
      { title, rating: Math.round(rating) }, // Round to ensure it's an integer
      { withCredentials: true }
    );
    console.log("Rating response:", response.data);
    // Fetch updated recipes
    this.fetchRecipes();
    alert("Rating updated successfully!");
  } catch (err) {
    console.error("Error updating rating:", err);
    alert("Failed to update rating.");
  }
};


  

  // IF FETCHING FROM API
    render() {
      const { searchTerm, recipes, error } = this.state;

  
      return (
        <div className="card" style={{ margin: "10px" }}>
          <div className="card-body">

          <input
          type="text"
          class="form-control"
          placeholder="Search recipes or ingredients..."
          value={searchTerm}
          onChange={this.handleInputChange}
          onKeyPress={this.handleKeyPress}
        />
        <button style={{ margin: "10px" }} onClick={this.handleSearch} className="btn btn-primary bt">Search</button>
        {this.state.error && <p style={{ color: "red" }}>{this.state.error}</p>}

            <div>
                {this.state.results.length > 0 ? (
                    this.state.results.map((recipe) => (
                        <div key={recipe.id} style={{ border: "1px solid #ccc", padding: "10px", margin: "10px 0" }}>
                            <h3>{recipe.title}</h3>
                            <p>Meal Type: {recipe.mealType}</p>
                            <p>Cooking Time: {recipe.cookingTime} minutes</p>
                            <p>Calories: {recipe.calories}</p>
                            <img src={recipe.image} alt={recipe.title} width="200" />
                        </div>
                    ))
                ) : (
                    <p>No results found</p>
                )}
            </div>  

            <h5 className="card-title">Recipes</h5>
            <p className="card-text">
              {error && <span style={{ color: "red" }}>{error}</span>}
            </p>
            <ul>
              {recipes.map((recipe, index) => (
                <li key={index}>
                  <h6>{recipe.recipe.label}</h6>
                  <img
                    src={recipe.recipe.image}
                    alt={recipe.recipe.label}
                    style={{ width: "400px" }}
                  />
                  <p>Calories: {Math.round(recipe.recipe.calories)}</p>
                  <p></p>
                  <p>Total time for making: {Math.round(recipe.recipe.totalTime)}</p>
                  <p></p>
                  <p>Meal type :
                  <ul>
                        {recipe.recipe.mealType.map((type, i) => (
                        <li key={i}>{type}</li>
                        ))}
                    </ul>
                    </p>
                  <p>Cuisine type:
                  <ul>
                        {recipe.recipe.cuisineType.map((type, i) => (
                        <li key={i}>{type}</li>
                        ))}
                    </ul>
                </p>

                       {/* Display Rating */}
                <div>
                  <p>Rating:</p>
                  {[1, 2, 3, 4, 5].map((star) => (
                    <FaStar
                      key={star}
                      size={20}
                      color={star <= (recipe.rating || 0) ? "gold" : "gray"}
                      onClick={() => this.handleRating(recipe.recipe.label, star)} // Use label or title   || recipe.title
                      style={{ cursor: "pointer" }}
                    />
                  ))}
                  <p>{recipe.rating && recipe.rating > 0 ? `${recipe.rating} stars` : "No rating yet"}</p>
                </div> 

                  {/* <p>List of ingredients:
                  <ul>
                        {recipe.recipe.ingredientLines.map((ingredient, i) => (
                        <li key={i}>{ingredient}</li>
                        ))}
                    </ul>
                </p> */}
                <p>List of ingredients for shopping list:
                  {/* <ul>
                        {recipe.recipe.ingredients.map((ingredient, i) => (
                        <li key={i}>{ingredient.food} {ingredient.quantity} {ingredient.measure}</li>
                        ))}
                    </ul> */}
                    <ul>
                      {recipe.recipe.ingredients.map((ingredient, i) => (
                          <li key={i}>
                              <input
                                  type="checkbox"
                                  id={`ingredient-${i}`}
                                  onChange={(e) => this.handleCheckboxChange(e, ingredient)}
                              />
                              <label htmlFor={`ingredient-${i}`}>
                                  {ingredient.food} {ingredient.quantity} {ingredient.measure}
                              </label>
                          </li>
                      ))}
                  </ul>
                  <button
                      className="btn btn-primary"
                      onClick={this.saveIngredientsToShoppingList}
                  >
                      Save to Shopping List
                  </button>
                </p>

                      {/* Comment Section */}
                      <div style={{ marginTop: "20px" }}>
                          <h4>Comments</h4>
                          <ul>
                            {this.state.comments[recipe.recipe.label]?.length > 0 ? (
                              this.state.comments[recipe.recipe.label].map((comment) => (
                                <li key={comment.commentID}>
                                  {comment.text} - <em>{new Date(comment.date).toLocaleString()}</em>
                                </li>
                              ))
                            ) : (
                              <p>No comments yet. Be the first to comment!</p>
                            )}
                          </ul>
                          <textarea
                            placeholder="Add a comment..."
                            value={this.state.newComment}
                            onChange={(e) => this.setState({ newComment: e.target.value })}
                          />
                          <button onClick={() => this.handleAddComment(recipe.recipe.label)}>Add Comment</button>
                      </div>


                <div>
                                <label>
                                    Select Day: 
                                    <select
                                        onChange={(e) =>
                                            this.setState({
                                                [`day_${recipe.recipe.label}`]: e.target.value,
                                            })
                                        }
                                        value={this.state[`day_${recipe.recipe.label}`] || ""}
                                    >
                                        <option value="">Select a day</option>
                                        <option value="Monday">Monday</option>
                                        <option value="Tuesday">Tuesday</option>
                                        <option value="Wednesday">Wednesday</option>
                                        <option value="Thursday">Thursday</option>
                                        <option value="Friday">Friday</option>
                                        <option value="Saturday">Saturday</option>
                                        <option value="Sunday">Sunday</option>
                                    </select>
                                </label>
                </div>
                <button className="btn btn-primary bt" onClick={() => this.saveToMealPlan(recipe.recipe.label)} disabled={!this.state[`day_${recipe.recipe.label}`]}>
    Save to Meal Plan
</button>
                </li>
              ))}
            </ul>
          </div>
        </div>
      );
    }


// IF FETCHING FROM DB
// render() {
//   const { searchTerm, recipes, error } = this.state;

//   return (
//       <div className="card" style={{ margin: "10px" }}>
//           <div className="card-body">
//               <input
//                   type="text"
//                   className="form-control"
//                   placeholder="Search recipes or ingredients..."
//                   value={searchTerm}
//                   onChange={this.handleInputChange}
//                   onKeyPress={this.handleKeyPress}
//               />
//               <button
//                   style={{ margin: "10px" }}
//                   onClick={this.handleSearch}
//                   className="btn btn-primary bt"
//               >
//                   Search
//               </button>
//               {error && <p style={{ color: "red" }}>{error}</p>}

//             <div>
//               {this.state.results.length > 0 ? (
//                     this.state.results.map((recipe) => (
//                         <div key={recipe.id} style={{ border: "1px solid #ccc", padding: "10px", margin: "10px 0" }}>
//                             <h3>{recipe.title}</h3>
//                             <p>Meal Type: {recipe.mealType}</p>
//                             <p>Total time for making: {recipe.cookingTime} minutes</p>
//                             <p>Calories: {recipe.calories}</p>
//                             <img src={recipe.image} alt={recipe.title} width="200" />
//                             <p>Date added: {recipe.dateAdded}</p>
//                         </div>
//                     ))
//                 ) : (
//                     <p>No results found</p>
//                 )}
//             </div>

//               <ul>
//                   {recipes.map((recipe) => (
//                       <li key={recipe.recipeID} style={{ border: "1px solid #ccc", padding: "10px", margin: "10px 0" }}>
//                           <h3>{recipe.title}</h3>
//                           <p>Date added: {recipe.dateAdded}</p>
//                           <p>Meal Type: {recipe.mealType}</p>
//                           <p>Total time for making: {recipe.cookingTime} minutes</p>
//                           <p>Calories: {recipe.calories}</p>
//                           <img src={recipe.image} alt={recipe.title} width="200" />

                              // {/* Display Rating */}   -- NEED TO BE TESTED !!
                              //   <div>
                              //   <p>Rating:</p>
                              //   {[1, 2, 3, 4, 5].map((star) => (
                              //     <FaStar
                              //       key={star}
                              //       size={20}
                              //       color={star <= recipe.rating ? "gold" : "gray"}
                              //       onClick={() => this.handleRating(recipe.recipeID, star)} // Update rating on click
                              //       style={{ cursor: "pointer" }}
                              //     />
                              //   ))}
                              //   <p>{recipe.rating ? `${recipe.rating} stars` : "No rating yet"}</p>
                              // </div>

//                           <p>Ingredients:</p>
//                           <ul>
//                               {recipe.ingredients.map((ingredient) => (
//                                   <li key={ingredient.ingredientID}>
//                                       <input
//                                           type="checkbox"
//                                           onChange={(e) => this.handleCheckboxChange(e, ingredient)}
//                                       />
//                                       {ingredient.name} - {ingredient.quantity} {ingredient.measure}
//                                   </li>
//                               ))}
//                           </ul>
//                           <button
//                               className="btn btn-primary"
//                               onClick={this.saveIngredientsToShoppingList}
//                           >
//                               Save Ingredients to Shopping List
//                           </button>

//                               {/* Comment Section */}
//                               <div style={{ marginTop: "20px" }}>
//                                 <h4>Comments</h4>
//                                 <ul>
//                                   {this.state.comments[recipe.recipeID]?.map((comment) => (
//                                     <li key={comment.commentID}>
//                                       {comment.text} - <em>{new Date(comment.date).toLocaleString()}</em>
//                                     </li>
//                                   ))}
//                                 </ul>
//                                 <textarea
//                                   placeholder="Add a comment..."
//                                   value={this.state.newComment}
//                                   onChange={(e) => this.setState({ newComment: e.target.value })}
//                                 />
//                                 <button onClick={() => this.handleAddComment(recipe.recipeID)}>Add Comment</button>
//                               </div>

//                           <div>
//                               <label>
//                                   Select Day:
//                                   <select
//                                       onChange={(e) =>
//                                           this.setState({
//                                               [`day_${recipe.title}`]: e.target.value,
//                                           })
//                                       }
//                                       value={this.state[`day_${recipe.title}`] || ""}
//                                   >
//                                       <option value="">Select a day</option>
//                                       <option value="Monday">Monday</option>
//                                       <option value="Tuesday">Tuesday</option>
//                                       <option value="Wednesday">Wednesday</option>
//                                       <option value="Thursday">Thursday</option>
//                                       <option value="Friday">Friday</option>
//                                       <option value="Saturday">Saturday</option>
//                                       <option value="Sunday">Sunday</option>
//                                   </select>
//                               </label>
//                           </div>
//                           <button
//                               className="btn btn-primary"
//                               onClick={() => this.saveToMealPlan(recipe.title)}
//                               disabled={!this.state[`day_${recipe.title}`]}
//                           >
//                               Save to Meal Plan
//                           </button>
//                       </li>
//                   ))}
//               </ul>
//           </div>
//       </div>
//   );
// }

  }

export default SearchView;