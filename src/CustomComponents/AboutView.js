import React from "react";

class AboutView extends React.Component{
    handleRedirect = () => {
        // Navigate to the signup page when the button is clicked
        this.props.QSetView({ page: "signup" }); // This will trigger the "SIGNUP" case in App.js
    };
    

    render(){
        return(
        <div className="card" 
             style={{margin:"10px"}}>
            <div className="card-body">
                <h5 className="card-title">About us</h5>
                <p className="card-text">Welcome to our Cooking Corner! We aim to make cooking
                simple, fun, and accessible for everyone.. </p>
                <hr></hr>
                <h4 className="text-secondary">Why This System?</h4>
                    <p>
                        Finding recipes that match your preferences, dietary needs, and
                        available ingredients can be frustrating. Cooking blogs and
                        cookbooks are often overwhelming, costly, and inaccessible to some
                        users.
                    </p>
                <hr></hr>
                <h4 className="text-secondary">Our Solution</h4>
                    <p>
                        Our platform simplifies the process by offering:
                    </p>
                    <ul>
                        <li>Personalized recipe searches based on your preferences.</li>
                        <li>Ingredient-based suggestions to minimize food waste.</li>
                        <li>Meal planning for better organization.</li>
                        <li>Storage for your personal recipes.</li>
                    </ul>
                <hr></hr>
                <h4 className="text-secondary">Join Us Today!</h4>
                    <p>
                    Start exploring recipes tailored to you, plan your meals, and make
                    cooking an enjoyable experience. Let’s get started today!
                    </p>
                <hr></hr>
                <p className="card-text">Do you want to join us?  </p>
                <button style={{ margin: "10px" }} onClick={this.handleRedirect}
                    className="btn btn-primary bt">YES !</button>
            </div>
        </div>
        )
    }
}

export default AboutView