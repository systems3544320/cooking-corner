import React from "react";
import axios from "axios";
import kitchenImage from './images/kitchen.jpg';
import { API_URL } from "../Utils/Configuration";

class HomeView extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
          likes: 0, // Initial state for likes
          dislikes: 0, // Initial state for dislikes
        };
      }
    
      // Method to handle "Like" button click
    //   handleLike = () => {
    //     this.setState((prevState) => ({
    //       likes: prevState.likes + 1,
    //     }));
    //   };
    // Handle like button click
    handleLike = () => {
        axios
            .post(
                `${API_URL}/users/pagePreference`,
                { user_input: "like" },
                { withCredentials: true }
            )
            .then(() => {
                this.fetchPagePreferenceCount();
            })
            .catch((err) => {
                console.error("Error submitting like:", err);
            });
    };
    
      // Method to handle "Dislike" button click
    //   handleDislike = () => {
    //     this.setState((prevState) => ({
    //       dislikes: prevState.dislikes + 1,
    //     }));
    //   };
    handleDislike = () => {
        axios
            .post(
                `${API_URL}/users/pagePreference`,
                { user_input: "dislike" },
                { withCredentials: true }
            )
            .then(() => {
                this.fetchPagePreferenceCount();
            })
            .catch((err) => {
                console.error("Error submitting dislike:", err);
            });
    };

      // Fetch likes and dislikes count
    fetchPagePreferenceCount = () => {
        axios
            .get(`${API_URL}/users/pagePreferenceCount`, { withCredentials: true })
            .then((response) => {
                const { likeCount, dislikeCount } = response.data;
                this.setState({ likes: likeCount, dislikes: dislikeCount });
            })
            .catch((err) => {
                console.error("Error fetching page preferences:", err);
            });
    };

    componentDidMount() {
        this.fetchPagePreferenceCount();
    }


    render() {
        const { likes, dislikes } = this.state;
    
        return (
          <div
            style={{
              //position: 'relative',
        
              margin: 0, // Remove any external margins
              padding: 0, // Remove any external padding
            }}
          >
            {/* Full-Screen Background Image */}
            <div
              style={{
                backgroundImage: `url(${kitchenImage})`,
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                backgroundRepeat: 'no-repeat',
                width: '100%',
                height: '100%',
                filter: 'blur(4px)', // Adds a blur effect
                position: 'absolute',
                top: 0, // Align to top of the viewport
                left: 0, // Align to left of the viewport
                zIndex: -1,
              }}
            ></div>
    
            {/* Welcome Content */}
            <div
              style={{
                position: 'relative',
                zIndex: 1,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                height: '100%',
                color: 'white',
                textAlign: 'center',
                marginTop: '100px', /* Ensure there's space at the top */
              }}
            >
              <div
                style={{
                  backgroundColor: 'rgba(0, 0, 0, 0.5)',
                  padding: '20px',
                  borderRadius: '10px',
                  maxWidth: '600px',
                  width: '80%',
                }}
              >
                <h1>Welcome to Our Cooking Corner!</h1>
                <p>
                  Discover recipes tailored to your preferences, plan your meals,
                  and make cooking fun and simple. Dive into a world of culinary
                  inspiration!
                </p>
                <div style={{ marginTop: '20px' }}>
                  <h3>Do you like our page?</h3>
                  <button
                    style={{
                      margin: '10px',
                      padding: '10px 20px',
                      fontSize: '16px',
                      backgroundColor: 'green',
                      color: 'white',
                      border: 'none',
                      borderRadius: '5px',
                      cursor: 'pointer',
                    }}
                    onClick={this.handleLike}
                  >
                    👍 Like ({likes})
                  </button>
                  <button
                    style={{
                      margin: '10px',
                      padding: '10px 20px',
                      fontSize: '16px',
                      backgroundColor: 'red',
                      color: 'white',
                      border: 'none',
                      borderRadius: '5px',
                      cursor: 'pointer',
                    }}
                    onClick={this.handleDislike}
                  >
                    👎 Dislike ({dislikes})
                  </button>
                </div>
              </div>
            </div>
          </div>
        );
      }
}

export default HomeView