export const ABOUT = 'about';  //?
// export const NOVICE = 'novice';
// export const ADDNEW = 'addnew';
export const SIGNUP = 'signup';   //ok
export const LOGIN = 'login';   //ok
// export const NOVICA = 'novica';
export const HOME = 'home';    //ok
export const UPLOAD = 'upload';  //ok
export const LOGOUT = 'logout';   //ok

// add profile search mealplan shoppinglist
export const PROFILE = 'profile';
export const SEARCH = 'search';
export const MEALPLAN = 'mealplan';
export const SHOPPINGLIST = 'shoppinglist';
