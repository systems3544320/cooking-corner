import { Component } from "react";
import { ABOUT, SIGNUP, LOGIN, HOME, LOGOUT, UPLOAD, PROFILE, SEARCH, MEALPLAN, SHOPPINGLIST } from "./Utils/Constants"
import HomeView from "./CustomComponents/HomeView";
import AboutView from "./CustomComponents/AboutView";
// import NoviceView from "./CustomComponents/NoviceView";
// import AddNovicaView from "./CustomComponents/AddNovicaView";
import SignupView from "./CustomComponents/SignupView";
import LoginView from "./CustomComponents/LoginView";
// import SingleNovicaView from "./CustomComponents/SingleNovicaView";
// add profile, ...
import ProfileView from "./CustomComponents/ProfileView";
import SearchView from "./CustomComponents/SearchView";
import MealplanView from "./CustomComponents/MealplanView";
import ShoppingListView from "./CustomComponents/ShoppingListView";

import FilesUploadComponent from "./CustomComponents/FilesUpload";
import axios from "axios";
import { API_URL } from "./Utils/Configuration";
import Cookies from 'universal-cookie';
import '@fortawesome/fontawesome-free/css/all.min.css';
import './App.css';
const cookies = new Cookies();

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      CurrentPage: HOME,
      Novica: 1,
      status: {
        success: null,
        msg: ""
      },
      user: null,
      isDarkMode: JSON.parse(localStorage.getItem('darkMode')) || false
    };
  }

  QGetView(state) {
    const { theme } = state;
    const page = state.CurrentPage;
    switch (page) {
      case ABOUT:
        return <AboutView QSetView={this.QSetView} />;  // QSetView={this.QSetView}
      // case NOVICE:
      //   return <NoviceView QIDFromChild={this.QSetView} />;
      // case ADDNEW:
      //   return <AddNovicaView />;
      case SIGNUP:
        return <SignupView />;
      case LOGIN:
        return <LoginView QUserFromChild={this.QSetLoggedIn} />;
      case LOGOUT:
        return <HomeView />;
      case UPLOAD:
        return <FilesUploadComponent />;
      // case NOVICA:
      //   return <SingleNovicaView data={state.Novica} QIDFromChild={this.QSetView} />;
      case PROFILE:
        return <ProfileView/>;
      case SEARCH:
        return <SearchView/>;
      case MEALPLAN:
        return <MealplanView/>;
      case SHOPPINGLIST:
        return <ShoppingListView/>;
      default:
        return <HomeView />;
    }
  };

  QSetView = (obj) => {
    this.setState(this.state.status = { success: null, msg: "" })

    console.log("QSetView");
    this.setState({
      CurrentPage: obj.page,
      Novica: obj.id || 0
    });
  };

  // Method to toggle dark/light mode
  toggleDarkMode = () => {
    this.setState(prevState => {
      const newMode = !prevState.isDarkMode;
      localStorage.setItem('darkMode', JSON.stringify(newMode));
      return { isDarkMode: newMode };
    });
  };

  render() {
    const { isDarkMode, CurrentPage } = this.state;
    return (
      <div id="APP" className={`box ${isDarkMode ? 'dark-mode' : 'light-mode'}`} style={{ width: '100vw', height: '100vh', ...(CurrentPage === HOME && { background: 'none' }) }}>
        <div id="menu" className="row">
          <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <div className="container-fluid">
              <a
                //onClick={() => this.QSetView({ page: "home" })}
                onClick={this.QSetView.bind(this, { page: "home" })}
                className="navbar-brand"
                href="#"
              >
                <i className="fas fa-home"></i> Home
              </a>
              <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>

              <div
                className="collapse navbar-collapse"
                id="navbarSupportedContent"
              >
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">

                <li className="nav-item">
                    <a
                      // onClick={() => this.QSetView({ page: ABOUT })}
                      onClick={this.QSetView.bind(this, { page: PROFILE })}
                      className="nav-link "
                      href="#"
                    >
                     <i className="fas fa-user"></i> Preferences
                    </a>
                  </li>

                  <li className="nav-item">
                  <a
                    onClick={this.QSetView.bind(this, { page: ABOUT })} // This sets page to "ABOUT"
                    className="nav-link"
                    href="#"
                  >
                     <i className="fas fa-info-circle"></i> About
                    </a>
                  </li>

                  {/* <li className="nav-item">
                    <a
                      // onClick={() => this.QSetView({ page: NOVICE })}
                      onClick={this.QSetView.bind(this, { page: NOVICE })}
                      className="nav-link "
                      href="#"
                    >
                      News
                    </a>
                  </li> */}

                  {/* <li className="nav-item">
                    <a
                      //onClick={() => this.QSetView({ page: ADDNEW })}
                      onClick={this.QSetView.bind(this, { page: ADDNEW })}
                      className="nav-link"
                      href="#"
                    >
                      Add news
                    </a>
                  </li> */}


                  <li className="nav-item">
                    <a
                      //onClick={() => this.QSetView({ page: ADDNEW })}
                      onClick={this.QSetView.bind(this, { page: UPLOAD })}
                      className="nav-link"
                      href="#"
                    >
                     <i className="fas fa-upload"></i> Upload
                    </a>
                  </li>

                  <li className="nav-item">
                    <a
                      //onClick={() => this.QSetView({ page: ADDNEW })}
                      onClick={this.QSetView.bind(this, { page: SEARCH })}
                      className="nav-link"
                      href="#"
                    >
                     <i className="fas fa-search"></i> Search
                    </a>
                  </li>

                  <li className="nav-item">
                    <a
                      //onClick={() => this.QSetView({ page: ADDNEW })}
                      onClick={this.QSetView.bind(this, { page: MEALPLAN })}
                      className="nav-link"
                      href="#"
                    >
                     <i className="fas fa-calendar-alt"></i> Meal Plan
                    </a>
                  </li>

                  <li className="nav-item">
                    <a
                      //onClick={() => this.QSetView({ page: ADDNEW })}
                      onClick={this.QSetView.bind(this, { page: SHOPPINGLIST })}
                      className="nav-link"
                      href="#"
                    >
                     <i className="fas fa-shopping-cart"></i> Shopping List
                    </a>
                  </li>

                  <li className="nav-item">
                    <a
                      //onClick={() => this.QSetView({ page: SIGNUP })}
                      onClick={this.QSetView.bind(this, { page: SIGNUP })}
                      className="nav-link "
                      href="#"
                    >
                     <i className="fas fa-user-plus"></i> Sign up
                    </a>
                  </li>

                 <li className="nav-item" ><a onClick={this.QSetView.bind(this, { page: LOGIN })}
                      className="nav-link " href="#"> <i className="fas fa-sign-in-alt"></i> Login </a>
                 </li>
                
                </ul>

                    {/* Add a toggle button for dark/light mode */}
                {CurrentPage !== HOME && (
                  <button onClick={this.toggleDarkMode} className="btn btn-secondary">
                    
                   {this.state.isDarkMode ? 'Light' : 'Dark'} Mode
                  </button>
                )}

              </div>
            </div>
          </nav>
        </div>

        <div id="viewer" className="row container">
          {this.QGetView(this.state)}
        </div>
      </div>
    );
  }
}

export default App;
